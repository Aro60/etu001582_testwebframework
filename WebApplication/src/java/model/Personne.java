/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import annotation.Url_class;
import annotation.Url_method;
import java.sql.Date;
import java.time.LocalDate;
import view.ModelView;


/**
 *
 * @author aro
 */

@Url_class(url = "perso")
public class Personne {
    String id;
    String nom;
    Date date_naissance;
    String sexe;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(Date date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Personne(){
        
    }
    
    public Personne(String id, String nom, Date date_naissance, String sexe) {
        setId(id);
        setNom(nom);
        setDate_naissance(date_naissance);
        setSexe(sexe);
    }
    
    @Url_method(url = "info")
    public ModelView getInfo(){
        
        ModelView m=new ModelView("info.jsp");
//        set the data to pass
        
        m.getData().put("personne", this);
        m.getData().put("presentation", "Hi everyone ,let me introduce myself");
        
        return m;
    }
    
    
    @Url_method(url = "form")
    public ModelView toForm(){
        return new ModelView("form.jsp");
    }
    
}
