
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="model.*"%>
<%
   Personne p=(Personne)request.getAttribute("personne");
   String hi=(String)request.getAttribute("presentation");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h4><%=hi%></h4>
        <p>My name is <%=p.getNom()%>, <%=p.getSexe()%>; I was born on <%=p.getDate_naissance()%></p>
    </body>
</html>
