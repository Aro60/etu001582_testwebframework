<%-- 
    Document   : form
    Created on : Nov 9, 2022, 8:26:35 AM
    Author     : aro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
    <form method="POST" action="perso-info.do">
        <input type="text" name="nom" value="Rakoto Modeste" />
        <select name="sexe">
            <option value="homme">Homme</option>
            <option value="femme">Femme</option>
        </select>

        <input type="date" name="date_naissance" value="2000-05-02"/>
        <input type="submit" value="OK" />
    </form>
        
    </body>
</html>
